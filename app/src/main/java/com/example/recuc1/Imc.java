package com.example.recuc1;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class Imc extends AppCompatActivity {

    private TextView textViewName;
    private EditText editTextWeight;
    private EditText editTextHeight;
    private TextView textViewResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imc);

        String name = getIntent().getStringExtra("name");

        textViewName = findViewById(R.id.textViewName);
        editTextWeight = findViewById(R.id.editTextWeight);
        editTextHeight = findViewById(R.id.editTextHeight);
        textViewResult = findViewById(R.id.textViewResult);

        textViewName.setText("Bienvenido, " + name);

        Button buttonCalculateIMC = findViewById(R.id.buttonCalculateIMC);
        buttonCalculateIMC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculateIMC();
            }
        });

        Button btnLimpiar = findViewById(R.id.btnLimpiar);
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearFields();
            }
        });

        Button btnRegresar = findViewById(R.id.btnRegresar);
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void calculateIMC() {
        String weightStr = editTextWeight.getText().toString();
        String heightStr = editTextHeight.getText().toString();

        if (weightStr.isEmpty() || heightStr.isEmpty()) {
            textViewResult.setText("Por favor, introduce tu peso y altura");
            return;
        }

        float weight = Float.parseFloat(weightStr);
        float height = Float.parseFloat(heightStr);

        float imc = calculateBMI(weight, height);
        String result = interpretIMC(imc);

        textViewResult.setText(result);
    }

    private float calculateBMI(float weight, float height) {
        return weight / (height * height);
    }

    private String interpretIMC(float imc) {
        return String.format("Tu IMC es: %.2f", imc);
    }

    private void clearFields() {
        editTextWeight.setText("");
        editTextHeight.setText("");
        textViewResult.setText("");
    }
}
