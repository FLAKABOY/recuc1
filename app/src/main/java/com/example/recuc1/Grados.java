package com.example.recuc1;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class Grados extends AppCompatActivity {

    private TextView textViewName;
    private EditText editTextGrados;
    private RadioGroup radioGroup;
    private Button buttonCalcular;
    private Button buttonLimpiar;
    private Button buttonRegresar;
    private TextView textViewResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grados);


        String name = getIntent().getStringExtra("name");

        textViewName = findViewById(R.id.textViewName);
        textViewName.setText("Bienvenido, " + name);

        editTextGrados = findViewById(R.id.editTextGrados);
        radioGroup = findViewById(R.id.radioGroup);
        buttonCalcular = findViewById(R.id.buttonCalcular);
        buttonLimpiar = findViewById(R.id.buttonLimpiar);
        buttonRegresar = findViewById(R.id.buttonRegresar);
        textViewResultado = findViewById(R.id.textViewResultado);

        buttonCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularConversion();
            }
        });

        buttonLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        buttonRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void calcularConversion() {
        String gradosStr = editTextGrados.getText().toString();

        if (gradosStr.isEmpty()) {
            Toast.makeText(this, "Por favor, introduce un valor en grados.", Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            float grados = Float.parseFloat(gradosStr);
            float resultado;

            RadioButton radioButtonCelsius = findViewById(R.id.radioButtonCelsius);

            if (radioButtonCelsius.isChecked()) {
                resultado = (grados * 9/5) + 32;
                textViewResultado.setText(grados + " grados Celsius son " + String.format("%.2f", resultado) + " grados Fahrenheit.");
            } else {
                resultado = (grados - 32) * 5/9;
                textViewResultado.setText(grados + " grados Fahrenheit son " + String.format("%.2f", resultado) + " grados Celsius.");
            }
        } catch (NumberFormatException e) {
            Toast.makeText(this, "Error en la conversión. Introduce un valor numérico válido.", Toast.LENGTH_SHORT).show();
        }
    }


    private void limpiarCampos() {
        editTextGrados.setText("");
        radioGroup.clearCheck();
        textViewResultado.setText("");
    }
}