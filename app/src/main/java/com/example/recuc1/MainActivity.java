package com.example.recuc1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private EditText editTextName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextName = findViewById(R.id.editTextName);
        Button buttonImc = findViewById(R.id.buttonIMC);
        buttonImc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImcActivity();
            }
        });
        Button buttonGrados = findViewById(R.id.buttonGrados);
        buttonGrados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGradosActivity();
            }
        });

        Button buttonSalir = findViewById(R.id.buttonSalir);
        buttonSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void openImcActivity() {
        String name = editTextName.getText().toString();
        if (!name.isEmpty()) {
            Intent intent = new Intent(this, Imc.class);
            intent.putExtra("name", name);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Por favor, ingresa tu nombre", Toast.LENGTH_SHORT).show();
        }
    }

    private void openGradosActivity() {
        String name = editTextName.getText().toString();
        if (!name.isEmpty()) {
            Intent intent = new Intent(this, Grados.class);
            intent.putExtra("name", name);
            startActivity(intent);
        } else {
            Toast.makeText(this, "Por favor, ingresa tu nombre", Toast.LENGTH_SHORT).show();
        }
    }


}
